import java.util.Arrays;

/**
 * @author Julien Troussier
 *
 *
 */

public class DoubleSymbolsPlayer extends Player {

    DoubleSymbolsPlayer(){
        super();
    }

    DoubleSymbolsPlayer(Integer baseWallet){
        super(baseWallet);
    }

    @Override
    protected boolean[] getKept(Machine.Symbols[] result) {

        boolean kept[] = new boolean[result.length];
        Arrays.fill(kept, false);

        Integer tmpDoubleSymbol = -1;
        for (int i=0 ; i<kept.length ; i++) {
            for (int j=0 ; j<kept.length ; j++) {
                if(j != i && result[i].ordinal() == result[j].ordinal()) {
                    if(tmpDoubleSymbol == -1) {
                        tmpDoubleSymbol = result[i].ordinal();
                    }
                    else {
                        tmpDoubleSymbol = (result[i].ordinal() > tmpDoubleSymbol)?result[i].ordinal():tmpDoubleSymbol;
                    }
                }
            }
        }

        for (int i=0 ; i<kept.length ; i++) {
            if (tmpDoubleSymbol != -1 && result[i].ordinal() == tmpDoubleSymbol) {
                kept[i] = true;
            }
        }

        return kept;
    }

    @Override
    public void updateReady() {
        super.updateReady();
    }
}
