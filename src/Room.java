import java.lang.reflect.Array;
import java.util.Vector;

/**
 * @author Jeremy Entressangle
 */
public class Room {
    private Place places[];
    private Vector<Player> leavePlayer = new Vector<>();

    /**
     *
     * @param nbMachine number of machine in the room
     */
    Room(int nbMachine) {
        places = new Place[nbMachine];
        for (int i = 0; i < places.length; i++) {
            places[i] = new Place();
        }
    }

    /**
     *
     * @param machines arrays of machines use in room
     */
    Room(Machine []machines) {
        places = new Place[machines.length];
        for (int i = 0; i < places.length; i++) {
            places[i] = new Place(machines[i]);
        }
    }

    /**
     *
     * @return number of machine in room
     */
    public int getNbMachine() {
        return places.length;
    }

    /**
     *
     * @return number of machine without player
     */
    public int getNbFreeMachine() {
        int result = 0;
        for (Place p: places) {
            if (p.isFree()) {
                result++;
            }
        }
        return result;
    }

    /**
     *
     * add player on room. The player use the first free machine
     * @param player Player add in room
     * @return Player can be add
     */
    public boolean newPlayer(Player player) {
        if (getNbFreeMachine() == 0) {
            return false;
        } else {
            int i = 0;
            while (!places[i].isFree()) {
                i++;
            }
            places[i].setPlayer(player);
            player.setMachine(places[i].getMachine());
            return true;
        }
    }

    /**
     * add players on room. Players use the first free machines
     * @param players Players add in room
     * @return Players can be add
     */
    public boolean newPlayer(Player []players) {
        if (players.length > getNbFreeMachine()) {
            return false;
        } else {
            for (Player p: players) {
                newPlayer(p);
            }
            return true;
        }
    }

    /**
     *
     * @return array of players in the room
     */
    public Player[] getPlayers() {
        Player [] result = new Player[getNbMachine() - getNbFreeMachine()];
        int playerFound = 0;
        for (int i = 0; i < getNbMachine(); i++) {
            if (!places[i].isFree()) {
                result[playerFound++] = places[i].getPlayer();
            }
        }
        return result;
    }


    /**
     *
     * @return players leave room
     */
    public Player[] getLeavePlayers() {
        Player []result = new Player[leavePlayer.size()];
        leavePlayer.toArray(result);
        return result;
    }

    /**
     *
     * @return players in room + players leave room
     */
    public Player[] getAllPlayers() {
        Player [] in = getPlayers();
        Player [] leave = getLeavePlayers();

        Player [] result = new Player[in.length + leave.length];
        System.arraycopy(in, 0, result, 0, in.length);
        System.arraycopy(leave, 0, result, in.length, leave.length);
        /*for (int i = 0; i < in.length; i++) {
            result[i] = in[i];
        }
        for (int i = 0; i < leave.length; i++) {
            result[in.length + i] = leave[i];
        }*/
        return result;
    }

    /**
     *
     * @return All machines in room
     */
    public Machine[] getMachines() {
        Machine []machines = new Machine[places.length];
        for (int i = 0; i < machines.length; i++) {
            machines[i] = places[i].getMachine();
        }
        return machines;
    }

    /**
     * Do action players in room (play, leave...)
     */
    private void playerLeave(Place place) {
        leavePlayer.add(place.getPlayer());
        place.getPlayer().setMachine(null);
        place.playerLeave();
    }

    /**
     * Do play all player in room
     */
    private void step() {
        for (Place place: places) {
            Player player = place.getPlayer();
            if (player != null) {
                if (player.getReady()) {
                    player.play();
                } else {
                    playerLeave(place);
                }
            }
        }
    }

    /**
     * Do play all player in room
     * @param number of steps
     */
    public void step(int number) {
        for (int i = 0; i < number; i++) {
            step();
        }
    }

    /**
     * A Place is 1 machine and 1 or 0 player
     */
    private class Place {
        private Machine machine;
        private Player player = null;

        Place() {
            machine = new Machine();
        }

        Place(Machine machine) {
            this.machine = machine;
        }

        public Machine getMachine() {
            return machine;
        }

        Player getPlayer() {
            return player;
        }

        void setPlayer(Player player) {
            this.player = player;
        }

        void playerLeave() { player = null; }

        boolean isFree() {
            return (player == null);
        }
    }

    /*public static void main(String []args) {
        Room room = new Room(10);
        for (int i = 0; i < 5; i++) {
            Player player = new BasicPlayer();
            room.newPlayer(player);
        }
        room.step(1000);

        Player []players = room.getPlayers();
        for (Player p: players) {
            System.out.println(p.getWallet());
        }
    }*/
}
