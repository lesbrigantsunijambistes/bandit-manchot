import java.util.Arrays;

/**
 * @author Julien Troussier
 */

public abstract class Player {

    private Machine machine;
    private Integer countIteration;
    private Integer wallet;
    private Integer baseWallet;
    private Integer maxWallet;
    private int     bet;
    private boolean ready;

    /**
     *  Default constructor instantiate Player with 500 tokens
     */
    Player(){
        baseWallet      = 500;
        wallet          = baseWallet;
        maxWallet       = baseWallet;
        bet             = 1;
        countIteration  = 0;
        updateReady();
    }

    /**
     *  Instantiate Player with configurable number of tokens
     *
     * @param _baseWallet
     */
    Player(Integer _baseWallet){
        baseWallet      = _baseWallet;
        wallet          = baseWallet;
        maxWallet       = baseWallet;
        bet             = 1;
        countIteration  = 0;
        updateReady();
    }

    /**
     *  Modify the wallet tokens
     *
     * @param wallet
     */
    protected void setWallet(Integer wallet) {
        this.wallet = wallet;
        updateMaxWallet();
        updateReady();
    }

    /**
     *
     * @return content of the wallet
     */
    public Integer getWallet() {
        return wallet;
    }

    /**
     *
     * @return content of the wallet at initialization
     */
    public Integer getBaseWallet() {
        return baseWallet;
    }

    /**
     *
     * @return maximum content of the wallet during the game
     */
    public Integer getMaxWallet() {
        return maxWallet;
    }

    /**
     *
     * @return player's bet
     */
    public int getBet() {
        return bet;
    }

    /**
     * Modify player's bet
     *
     * @param bet
     */
    public void setBet(int bet) {
        this.bet = bet;
        updateReady();
    }

    /**
     *
     * @return number of iteration
     */
    public Integer getCountIteration() {
        return countIteration;
    }

    /**
     *  Modify maximum content of the wallet during the game
     */
    private void updateMaxWallet() {
        if(wallet > maxWallet) {
            maxWallet = wallet;
        }
    }

    /**
     * Is used in the child class to change the game stop conditions
     *
     * @param ready
     */
    protected void setReady(boolean ready) {
        this.ready = ready;
    }

    /**
     *
     * @return Player's stop conditions
     */
    public boolean getReady() {
        return ready;
    }

    /**
     * Modify the machine to sign the player
     *
     * @param machine
     */
    public void setMachine(Machine machine) {
        this.machine = machine;
        updateReady();
    }

    /**
     *
     * @return The machine to sign the player
     */
    protected Machine getMachine() {
        return this.machine;
    }

    /**
     * This method communicates with the machine that is assigned to the player
     * and plays a part
     */
    public void play(){
        if(getReady()) {
            countIteration++;

            boolean kept[] = new boolean[machine.getNumberOfReels()];
            Arrays.fill(kept, false);

            setWallet(wallet - bet);
            machine.addTokens(bet);
            int gain = machine.play(kept);

            if(gain > 0) {
                setWallet(wallet + gain);
            }
            else if(gain == 0) {
                Machine.Symbols[] result = machine.getResult();
                kept = getKept(result);
                gain = machine.play(kept);
                if(gain > 0) {
                    setWallet(wallet + gain);
                }
            }
        }

        updateReady();
    }

    /**
     * This method need to be redefines in child class
     *
     * @param result gived by the machine
     * @return The rollers to keep
     */
    abstract protected boolean[] getKept(Machine.Symbols [] result);

    /**
     * Update player's stop conditions
     */
    public void updateReady() {
        if ((getWallet()-getBet()) < 0 || getMachine() == null || getWallet() <= 0) {
           setReady(false);
        }
        else {
            setReady(true);
        }
    }
}
