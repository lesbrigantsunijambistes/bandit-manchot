import java.util.Arrays;

/**
 * @author Julien Troussier
 *
 *
 */

public class HighSymbolsPlayer extends Player {

    HighSymbolsPlayer(){
        super();
    }

    HighSymbolsPlayer(Integer baseWallet){
        super(baseWallet);
    }

    @Override
    protected boolean[] getKept(Machine.Symbols[] result) {
        boolean kept[] = new boolean[result.length];
        Arrays.fill(kept, false);

        //Set the high symbol on result
        Integer tmpHighSymbol = 0;
        for (int i=0 ; i<kept.length ; i++) {
            if (tmpHighSymbol < result[i].ordinal()) {
                tmpHighSymbol = result[i].ordinal();
            }
        }

        //Set true on kept array when the roll is on the high symbol
        for (int i=0 ; i<kept.length ; i++) {
            if (result[i].ordinal() == tmpHighSymbol) {
                kept[i] = true;
            }
        }

        return kept;
    }

    @Override
    public void updateReady() {
        super.updateReady();
    }
}
