/**
 * @authors Quentin Fabre, Jeremy Entressangle
 */

class StatisticsTest extends GroovyTestCase {
    void testSetPlayers() {
    }

    void testTypesBehavior() {
        final int Step = 1
        Map<Machine.Symbols, Integer> gain = new HashMap<>()
        gain.put(Machine.Symbols.cherry, 5)
        Map<Machine.Symbols, Integer> symbols = new HashMap<>()
        symbols.put(Machine.Symbols.cherry, 1)
        Machine machine = new Machine(gain, symbols)
        Machine []machines = [machine]

        Room room = new Room(machines)
        room.newPlayer(new BasicPlayer())
        room.step(Step)

        Statistics stats = new Statistics()
        stats.setPlayers(room.getAllPlayers())
        //Statistics.Results result = stats.result("BasicPlayer")

        /*assert result.bets == 1*Step
        assert result.gains == 4*Step
        assert result.shots == 1*Step
        assert result.tokens == -4*/
    }
}
