/**
 * @author Thibaut Jallois
 * 
 *Provides the classes necessary to create an fake gambling machine.
 *<p>
 *This class allow to test player's behavior
 *<p>
 */
public class FakeMachine extends  Machine{
	
	
	private boolean[] tab = new boolean[this.numberOfReels];
	
	public boolean[] getTab() {
		return tab;
	}
	public void setResult(Symbols[] arrayOfSymbols) {
		result = arrayOfSymbols;
	}
	@Override
	public int play(boolean[] tab) {
		this.tab = tab;
		return 0;
	}
	

}
