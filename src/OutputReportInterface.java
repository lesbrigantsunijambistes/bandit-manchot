import java.util.List;


import java.net.URI;

/**
 * Interface who generate report
 * 
 * @author Thibaut Jallois
 */
public interface OutputReportInterface {
	/**
	 * make report with all statistic fir a type of player and for the casino
	 * @param StatByPlayer list of statistic for each type of player
	 */
	public void generateReport(List<String> StatByPlayer);

	/**
	 * Give URI to report
	 * @return
	 */
	public URI getIndexUri();

}