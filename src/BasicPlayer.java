import java.util.Arrays;

/**
 * @author Julien Troussier
 *
 *
 */

public class BasicPlayer extends Player{

    BasicPlayer(){
        super();
    }

    BasicPlayer(Integer baseWallet){
        super(baseWallet);
    }

    @Override
    protected boolean[] getKept(Machine.Symbols[] result) {
        boolean kept[] = new boolean[result.length];
        Arrays.fill(kept, false);

        return kept;
    }

    @Override
    public void updateReady() {
        super.updateReady();

        if(getWallet() >= getBaseWallet()*2.0f) {
            setReady(false);
        }
    }
}
