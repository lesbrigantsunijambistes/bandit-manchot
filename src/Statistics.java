import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Quentin Fabre
 */

public class Statistics{

    private static int sumFinalTokensMachines;

    List<String> outPut = new ArrayList<>();

    public OutputTextReport getOutPutR() {
        return outPutR;
    }

    private OutputTextReport outPutR;

    private String []type = {
            "BasicPlayer",
            "SevenMasterPlayer",
            "HighSymbolsPlayer",
            "DoubleSymbolsPlayer",
            "MadDoubleHighSymbolsPlayer"
    };

    private static Map<String, Integer> nb_TypesPlayer = new HashMap<>();
    private static Map<String, Integer> gain_TypesPlayer = new HashMap<>();
    private static Map<String, Integer> shots_TypesPlayer = new HashMap<>();
    private static Map<String, Integer> bets_TypesPlayer = new HashMap<>();

    /**
     * Setup map number types players
     */
     Statistics(){
         initTypesMap(type);
     }

    /**
     * Setup map number types players
     * @param types all types of behavior
     */
    Statistics(String [] types){
        initTypesMap(types);
    }

    /**
     * Setup map number types players
     * @param type all types of behavior
     */
    private void initTypesMap(String[] type) {
        for(String t: type) {
            nb_TypesPlayer.put(t, 0);
            gain_TypesPlayer.put(t, 0);
            shots_TypesPlayer.put(t, 0);
            bets_TypesPlayer.put(t, 0);
        }
    }

    /**
     * Calculation stats of each type of players
     * @param player array of all player in the room
     */
    public void setPlayers(Player [] player){
        for(int i = 0 ; i < player.length ; i++){
            if(player[i].getClass() == BasicPlayer.class){
                initMapBehavior(player[i], player[i].getClass().getName());
            }
            else if(player[i].getClass() == SevenMasterPlayer.class){
                initMapBehavior(player[i], player[i].getClass().getName());
            }
            else if(player[i].getClass() == HighSymbolsPlayer.class){
                initMapBehavior(player[i], player[i].getClass().getName());
            }
            else if(player[i].getClass() == DoubleSymbolsPlayer.class){
                initMapBehavior(player[i], player[i].getClass().getName());
            }
            else if(player[i].getClass() == MadDoubleHighSymbolsPlayer.class){
                initMapBehavior(player[i], player[i].getClass().getName());
            }
        }
    }

    /**
     * Calculation stats of each final tokens of machine
     * @param machine oarray of all machine in the room
     */
    public float setMachines(Machine [] machine){
        for(int i = 0 ; i < machine.length ; i++){
            sumFinalTokensMachines += (machine[i].getTokens() - 1200);
        }
        return sumFinalTokensMachines;
    }

    /**
     * Classification maps
     * @param p one player
     * @param type type of this player
     */
    private void initMapBehavior(Player p, String type) {
        nb_TypesPlayer.put(type, nb_TypesPlayer.get(type) + 1);
        gain_TypesPlayer.put(type, gain_TypesPlayer.get(type) + (p.getWallet() - p.getBaseWallet()));
        shots_TypesPlayer.put(type, shots_TypesPlayer.get(type) + p.getCountIteration());
        bets_TypesPlayer.put(type, bets_TypesPlayer.get(type) + p.getBet());
    }

    /**
     * Money bet by one type of behavior
     * @param typePlayer type of behavior
     */
    private float getAverageBetByTypesBehavior(String typePlayer){
        return (bets_TypesPlayer.get(typePlayer) / nb_TypesPlayer.get(typePlayer));
    }

    /**
     * Shots for one type of behavior
     * @param typePlayer type of behavior
     */
    private float averageTestShotsByTypesBehavior(String typePlayer){
        return shots_TypesPlayer.get(typePlayer) / nb_TypesPlayer.get(typePlayer);
    }

    /**
     * Calculation of the average gain of one behavior
     * @param typePlayer type of behavior
     */
    private float averageGainByTypesBehavior(String typePlayer){
        return gain_TypesPlayer.get(typePlayer) / nb_TypesPlayer.get(typePlayer);
    }

    /**
     * Return a list of all stats by behavior7+
     * @param
     */
    public void intiListOutPut(){
        for(String t: type) {
            outPut.add("Type de comportement : " + t + "\n");
            outPut.add("Nombre : " + nb_TypesPlayer.get(t) + "\n");
            String avgBets = String.valueOf(getAverageBetByTypesBehavior(t));
            outPut.add("Moyenne Misé : " + avgBets + "\n");
            String avgGains = String.valueOf(averageGainByTypesBehavior(t));
            outPut.add("Moyenne Solde : " + avgGains + "\n");
            String avgShots = String.valueOf(averageTestShotsByTypesBehavior(t));
            outPut.add("Moyenne Coups d'essais : " + avgShots + "\n\n");
        }
        String sumTmp = String.valueOf(sumFinalTokensMachines);
        outPut.add("Banque totale : " + sumTmp);

        outPutR = new OutputTextReport();
        outPutR.generateReport(outPut);
    }
}