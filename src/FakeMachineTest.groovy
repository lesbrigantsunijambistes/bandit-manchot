/**
 * @author Jeremy Entressangle
 */

class FakeMachineTest extends GroovyTestCase {
    void testPlayAndGetTab() {
        boolean [][] setOfTests = [
                [true,true,false],
                [true,false,true],
                [false,true,true]
        ]
        for (boolean [] test: setOfTests) {
            FakeMachine machine = new FakeMachine()
            machine.play(test)
            assert machine.getTab() == test
        }
    }

    void testSetResult() {
        Machine.Symbols [][] setOfTest = [
                [Machine.Symbols.cherry, Machine.Symbols.cherry, Machine.Symbols.bell],
                [Machine.Symbols.cherry, Machine.Symbols.bell, Machine.Symbols.bell],
                [Machine.Symbols.bell, Machine.Symbols.cherry, Machine.Symbols.bell]
        ]

        for (Machine.Symbols [] test: setOfTest) {
            FakeMachine machine = new FakeMachine()
            machine.setResult(test)
            machine.play()
            for (int i; i < 3; i++) {
                assert machine.getResult()[i] == test[i]
            }
        }
    }
}
