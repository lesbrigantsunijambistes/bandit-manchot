class MadDoubleHighSymbolsPlayerTest extends GroovyTestCase {
    void testGetKept() {
        FakeMachine machine = new FakeMachine()

        Machine.Symbols [][] setOfTest = [
                [Machine.Symbols.seven, Machine.Symbols.seven, Machine.Symbols.bell],
                [Machine.Symbols.cherry, Machine.Symbols.bell, Machine.Symbols.watermelon],
                [Machine.Symbols.bell, Machine.Symbols.cherry, Machine.Symbols.bell]
        ]

        boolean [][] setOfResult = [
                [true, true, false],
                [false, true, false],
                [true, false, true]
        ]

        for (int i=0 ; i<setOfTest.length ; i++) {
            def p = new MadDoubleHighSymbolsPlayer(10)
            p.setMachine(machine)
            machine.setResult(setOfTest[i])
            p.play()
            def kept = machine.getTab();
            //assert Arrays.equals(kept, setOfResult[i])
        }
    }

    void testDefaultConstruct() {
        def p = new MadDoubleHighSymbolsPlayer()
        assert p.getBaseWallet() == 500 && p.getBet() == p.getBaseWallet()
    }

    void testIsReadyToEnd() {
        def p = new MadDoubleHighSymbolsPlayer(10)
        FakeMachine machine = new FakeMachine()
        p.setMachine(machine)
        machine.setResult([Machine.Symbols.seven, Machine.Symbols.seven, Machine.Symbols.bell] as Machine.Symbols[])

        assert p.getReady()
        p.play()
        assert !p.getReady()
    }
}
