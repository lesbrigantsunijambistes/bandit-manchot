
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * @author Thibaut Jallois
 * 
 *Provides the classes necessary to create an gambling machine.
 *<p>
 *This class simulate behavior of a slot machine (Tokens, random generation of symbols...)
 *<p>
 */
public class Machine {

	/**
	 * symbols for a slot 
	 * 
	 *
	 */
	public enum  Symbols{
		cherry,
		watermelon,
		lemon,
		bar, 
		bell,
		diamond,
		seven
	}

	protected int numberOfReels = 3;         
	private int tokens = 1200;
	protected Symbols[] result = new Symbols[numberOfReels];
	private int bet = 0;

	private Random randomGenerator = new Random();
	private boolean isPayed = false;
	private boolean isFirstRound;
	/**
	 * map who contains pair<Symbols.ordinal(), amount of tokens>
	 * to retrieve easily amount of gain for a result
	 */
	private static Map<Symbols,Integer> gain;
	/**
	 * simulate reels with the number of occurence for each Symbol
	 */
	private List<Symbols> reels;   


	/**
	 * @param mapSymbolsGain store amount of gain for a symbols
	 * @param mapSymbolsOccurences list of symbols for a slot
	 */
	Machine(Map<Symbols, Integer> mapSymbolsGain, Map<Symbols,Integer> mapSymbolsOccurences) { 
		reels = createReels(mapSymbolsOccurences);
		gain = mapSymbolsGain;
	}


	public static Symbols stringToSymbols(String str) {
		for(Symbols s : Symbols.values()) {
			if(str.equals(s.toString())) {
				return s;
			}
		}
		return null;
	}

	/**
	 * default constructor instantiate machine with static gain arrays and reels list
	 * 
	 */
	Machine(){
		Map<Symbols,Integer> mreel = new HashMap<Symbols,Integer>();
		mreel.put(Symbols.cherry, 7);
		mreel.put(Symbols.watermelon, 6);
		mreel.put(Symbols.lemon, 5);
		mreel.put(Symbols.bar, 4);
		mreel.put(Symbols.bell, 3);
		mreel.put(Symbols.diamond, 2);
		mreel.put(Symbols.seven, 2);
		reels = createReels(mreel);
		gain = new HashMap<Symbols,Integer>();
		gain.put(Symbols.cherry, 3);
		gain.put(Symbols.watermelon, 8);
		gain.put(Symbols.lemon, 15);
		gain.put(Symbols.bar, 20);
		gain.put(Symbols.bell, 70);
		gain.put(Symbols.diamond, 150);
		gain.put(Symbols.seven, 300);
	}


	/**
	 * @param tab
	 * @return gain or -1 if the machine doesn't have tokens
	 */
	public int play(boolean[] tab) {
		if(tokens>0) {
			if(isFirstRound && isPayed) {
				boolean[] a = new boolean[numberOfReels];
				Arrays.fill(a, false);
				Draw(a);	
			}
			else if(!isFirstRound) {
				Draw(tab);
				isPayed = false;
			}

			int g = checkGain();
			isFirstRound = (g>0||!isFirstRound)?true:false;
			return g;
		}
		else return -1;
	}


	/**
	 * Draw random result 
	 *
	 *
	 * @param kept contains if a slot is kept or not
	 */
	private void Draw(boolean[]kept){
		if(kept.length != numberOfReels){
			System.err.println("Caught SizeExecption: size of boolean[] kept != "+numberOfReels);
		}
		else{
			for(int i=0 ; i<kept.length;i++){
				if(!kept[i]){
					DrawASLot(i);
				}
			}
		}
	}

	/**
	 * Draw random result for specific slot
	 * @param indexOfaSlot index of a slot
	 */
	private void DrawASLot(int indexOfaSlot) {
		result[indexOfaSlot] = reels.get(randomGenerator.nextInt(reels.size()));
	}


	/**
	 * @return amount of tokens for given result
	 */
	private int checkGain() {
		List<Symbols> list = Arrays.asList(result);
		int c = 0;
		Symbols index = null ;
		for (int i = 0; i < result.length-1; i++) {
			if (Collections.frequency(list, result[i]) > c) {
				c = Collections.frequency(list, result[i]);
				index = result[i];
			}
		}
		if (c==numberOfReels) {
			return withdrawTokens(gain.get(index)*bet);
		}
		else return 0;
	}


	/** 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Machine [result=" + Arrays.toString(result) + "]";
	}

	/**
	 * @return number of slot for a machine
	 */
	public  int getNumberOfReels() {
		return numberOfReels;
	}

	public void setNumberOfReels(int numberOfReels) {
		this.numberOfReels = numberOfReels;
		result = new Symbols[numberOfReels];
	}

	/**
	 * @return amount of tokens for a machine
	 */
	public int getTokens() {
		return this.tokens;
	}
	/**
	 * @param token number of token subtracted
	 */
	private int withdrawTokens(int token) {
		if(tokens-token>=0) {
			tokens -=  token;
			return token;
		}
		else {
			int gain = tokens;
			tokens = 0;
			return gain;
		}
	}

	/**
	 * @param token number of token put in the machine
	 */
	public void addTokens(int token) {
		if(token>0) {
			bet = token;
			isPayed = true;
			tokens += token;
		}
	}
	/**
	 * Getter for member result
	 * @return result
	 */
	public Symbols[] getResult() {
		return result;
	}

	/**
	 * @param mapofSymbolsOccurence who contain symbols and number associate for a slot
	 * @return a list of symbols
	 */
	private List<Symbols> createReels(Map<Symbols,Integer> mapofSymbolsOccurence){
		List<Symbols> list = new ArrayList<Symbols>();;
		for(Symbols i : mapofSymbolsOccurence.keySet()) {
			for(int b = 0; b<mapofSymbolsOccurence.get(i);b++ ) {
				list.add(i);
			}
		}
		Collections.shuffle(list);
		return list;
	}

}
