import JSON.JSONArray;
import JSON.JSONObject;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Jeremy Entressangle
 */


public class Casino {
    private Room room;
    private Statistics stats = new Statistics();;

    Casino(String path) throws IOException {
        config(path);
    }

    /**
     *
     * @param path to configuration.json file
     * @throws IOException
     */
    private void config(String path) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        String json = new String(encoded, Charset.defaultCharset());
        JSONObject obj = new JSONObject(json);


        JSONArray jsonPlayers = obj.getJSONArray("players");
        int nbPlayer = 0;
        for (int i = 0; i < jsonPlayers.length(); i++) {
            nbPlayer += jsonPlayers.getJSONObject(i).getInt("number");
        }

        JSONArray jsonSymbols = obj.getJSONObject("machines").getJSONArray("Symbols");
        Machine[] machines = initMachines(jsonSymbols, nbPlayer);
        room = new Room(machines);
        Player[] players = initPlayers(jsonPlayers, nbPlayer);
        room.newPlayer(players);

        int nbStep = obj.getJSONObject("casino").getInt("step");
        step(nbStep);
        stats.setPlayers(players);
        stats.setMachines(machines);
        stats.intiListOutPut();
    }

    /**
     *
     * @param jsonSymbols
     * @param nbPlayer
     * @return Machine[] contain configured machine
     */
    private Machine[] initMachines(JSONArray jsonSymbols, int nbPlayer) {

        Map<Machine.Symbols, Integer> gains = new HashMap<>();
        Map<Machine.Symbols, Integer> symbols = new HashMap<>();

        for (int i = 0; i< jsonSymbols.length(); i++) {
            JSONObject jsonSymbol = jsonSymbols.getJSONObject(i);
            Machine.Symbols type = Machine.stringToSymbols(jsonSymbol.getString("name"));
            int gain = jsonSymbol.getInt("gain");
            int occurrence = jsonSymbol.getInt("occurrence");
            gains.put(type, gain);
            symbols.put(type, occurrence);
        }

        Machine []machines = new Machine[nbPlayer];
        for (int i = 0; i < machines.length; i++) {
            machines[i] = new Machine(gains, symbols);
        }
        return machines;
    }

    /**
     *
     * @param jsonPlayers
     * @param nbPlayer
     * @return Player[] contain configured player
     */
    private Player[] initPlayers(JSONArray jsonPlayers, int nbPlayer) {
        Player []players = new Player[nbPlayer];
        int indexPlayer = 0;
        for (int i = 0; i < jsonPlayers.length(); i++) {
            String type = jsonPlayers.getJSONObject(i).getString("type");
            int number = jsonPlayers.getJSONObject(i).getInt("number");

            for (int j = 0; j < number; j++) {
                if (type.equals("BasicPlayer")) {
                    players[indexPlayer++] = new BasicPlayer();
                } else if (type.equals("DoubleSymbolsPlayer")) {
                    players[indexPlayer++] = new DoubleSymbolsPlayer();
                } else if (type.equals("HighSymbolsPlayer")) {
                    players[indexPlayer++] = new HighSymbolsPlayer();
                } else if (type.equals("MadDoubleHighSymbolsPlayer")) {
                    players[indexPlayer++] = new MadDoubleHighSymbolsPlayer();
                }  else if (type.equals("SevenMasterPlayer")) {
                    players[indexPlayer++] = new SevenMasterPlayer();
                } else {
                    //TODO ERROR
                }
            }
        }
        return players;
    }

    /**
     * Number of step play in room
     * @param number
     */
    private void step(int number) {
            room.step(number);
    }

    public Statistics getStats() {
        return stats;
    }

    public static void main(String []args) throws IOException {
        Casino casino = new Casino("config.json");
        System.out.println("Statistics file created : " + casino.getStats().getOutPutR().getM_path_to_file());
    }
}
