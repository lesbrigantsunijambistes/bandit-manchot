/**
 * @author Jeremy Entressangle
 */
class MachineTest extends GroovyTestCase {
    void testPlay() {
        // [tokens, Gain]
        int[][] setOfTests = [[1,5],
                         [3,15],
                         [0,0],
                         [1000, 2200]]

        for(int []t: setOfTests) {
            Map<Machine.Symbols, Integer> gain = new HashMap<>()
            gain.put(Machine.Symbols.cherry, 5);
            Map<Machine.Symbols, Integer> symbols = new HashMap<>()
            symbols.put(Machine.Symbols.cherry, 1);
            Machine machine = new Machine(gain, symbols)
            boolean [] v = [false, false, false]
            machine.addTokens(t[0])
            assert machine.play(v) == t[1]
        }
    }

    void testToString() {
    }

    void testGetDefaultNumberOfReels() {
        Machine machine = new Machine()
        assert machine.getNumberOfReels() == 3
    }

    void testAddAndGetTokens() {
        Machine machine = new Machine()
        int tockenBegin = machine.getTokens()
        machine.addTokens(3)
        assert machine.getTokens() == tockenBegin + 3
    }

    void testGetResultReturn() {
        Machine machine = new Machine()
        assert machine.getResult().length == machine.getNumberOfReels()
    }

    void testNumberOfReels() {
        Machine machine = new Machine()
        machine.setNumberOfReels(8)
        assert machine.getNumberOfReels() == 8
        machine.setNumberOfReels(12)
        assert machine.getNumberOfReels() == 12
    }

    void testStringToSymbols() {
        def setOfTests = [
                [Machine.Symbols.cherry, "cherry"],
                [Machine.Symbols.bell, "bell"],
                [Machine.Symbols.seven, "seven"],
                [Machine.Symbols.bar, "bar"],
                [null, "couscous"]
        ]
        for (def test: setOfTests) {
            assert Machine.stringToSymbols(test[1] as String) == test[0]
        }
    }
}
