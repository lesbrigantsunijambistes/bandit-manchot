import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.commons.io.FileUtils;
/**
 * @author Thibaut
 * 
 *Class who simply write statistic in a file
 */
public class OutputTextReport implements OutputReportInterface {

	public File getM_path_to_file() {
		return m_path_to_file.toFile();
	}

	private Path m_path_to_file = null;
	String m_time  = null;
	
	OutputTextReport(){
		
	}

	/* (non-Javadoc)
	 * @see OutputReportInterface#generateReport(java.util.List)
	 */
	@Override
	public void generateReport(List<String> StatByPlayer) {
		m_time = new SimpleDateFormat("dd-MM-HH-mm-ss").format(new Date());
		m_path_to_file = Paths.get("report" + File.separator + m_time + "report.txt");
		StatByPlayer.stream().forEach(string->{
			try {
				FileUtils.writeStringToFile(m_path_to_file.toFile(),string,"UTF-8",true);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
				);

	}

	/* (non-Javadoc)
	 * @see OutputReportInterface#getIndexUri()
	 */
	@Override
	public URI getIndexUri() {
		return m_path_to_file.toUri();
	}

}
