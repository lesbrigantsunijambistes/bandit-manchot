import java.util.Arrays;

/**
 * @author Julien Troussier
 *
 *
 */

public class SevenMasterPlayer extends Player {

    private boolean sevenIsHere = false;

    SevenMasterPlayer(){
        super();
        setBet(7);
    }

    SevenMasterPlayer(Integer baseWallet){
        super(baseWallet);
        setBet(7);
    }

    @Override
    protected boolean[] getKept(Machine.Symbols[] result) {
        boolean kept[] = new boolean[result.length];
        Arrays.fill(kept, false);

        for (int i=0 ; i<kept.length ; i++){
            if(result[i].ordinal() == Machine.Symbols.seven.ordinal()){
                kept[i] = true;
            }
        }

        //Set sevenIsHere at true if all the rollers are on seven
        boolean fullSeven[] = new boolean[kept.length];
        Arrays.fill(fullSeven, true);
        if(Arrays.equals(kept, fullSeven)){
            sevenIsHere = true;
        }

        return kept;
    }

    @Override
    public void updateReady() {
        super.updateReady();

        if(sevenIsHere == true) {
            setReady(false);
        }
    }
}
