/**
 * @author Jeremy Entressangle
 */

class RoomTest extends GroovyTestCase {
    void testGetNbMachine() {
        def room = new Room(8)
        assert room.getNbMachine() == 8
    }

    void testGetNbFreeMachine() {
        def room = new Room(12)
        assert room.getNbFreeMachine() == 12
        assert room.newPlayer(new BasicPlayer())
        assert room.getNbFreeMachine() == 11
    }

    void testNewPlayerWhenNoPlaceFree() {
        def room = new Room(2)
        assert room.newPlayer(new BasicPlayer())
        assert room.newPlayer(new BasicPlayer())
        assert !room.newPlayer(new BasicPlayer())
    }

    void testNewPlayers() {
        def room = new Room(2)
        Player []players = [new BasicPlayer(), new BasicPlayer()]
        assert room.newPlayer(players)
        assert room.nbFreeMachine == 0
        assert room.getPlayers().size() == 2

        def room2 = new Room(1)
        Player []players2 = [new BasicPlayer(), new BasicPlayer()]
        assert !room2.newPlayer(players2)
        assert room2.nbFreeMachine == 1
        assert room2.getPlayers().size() == 0
    }

    void testLeavePlayer() {
        Map<Machine.Symbols, Integer> gain = new HashMap<>()
        gain.put(Machine.Symbols.cherry, 0)
        Map<Machine.Symbols, Integer> symbols = new HashMap<>()
        symbols.put(Machine.Symbols.cherry, 1)
        Machine []machines = [new Machine(gain, symbols), new Machine(gain, symbols), new Machine(gain, symbols)]
        Room room = new Room(machines)

        Player []players = [new BasicPlayer(5), new BasicPlayer(10), new BasicPlayer(200)]
        room.newPlayer(players)

        room.step(100)

        assert room.getLeavePlayers().size() == 2
        assert room.getLeavePlayers().contains(players[0])
        assert room.getLeavePlayers().contains(players[1])
        assert room.getPlayers().size() == 1
    }

    void testAllPlayer() {
        Map<Machine.Symbols, Integer> gain = new HashMap<>()
        gain.put(Machine.Symbols.cherry, 0)
        Map<Machine.Symbols, Integer> symbols = new HashMap<>()
        symbols.put(Machine.Symbols.cherry, 1)
        Machine []machines = [new Machine(gain, symbols), new Machine(gain, symbols)]
        Room room = new Room(machines)

        Player []players = [new BasicPlayer(100), new BasicPlayer(5)]
        room.newPlayer(players)

        room.step(50)

        assert room.getLeavePlayers().size() == 1
        assert room.getPlayers().size() == 1
        assert room.getAllPlayers().size() == 2
        assert room.getAllPlayers().contains(players[0])
        assert room.getAllPlayers().contains(players[1])
    }

    void testPlayerWin() {
        Map<Machine.Symbols, Integer> gain = new HashMap<>()
        gain.put(Machine.Symbols.cherry, 5)
        Map<Machine.Symbols, Integer> symbols = new HashMap<>()
        symbols.put(Machine.Symbols.cherry, 1)
        Machine machine = new Machine(gain, symbols)
        Machine []machines = [machine]
        Room room = new Room(machines)

        Player player = new BasicPlayer()
        room.newPlayer(player)
        room.step(2)

        assert player.getWallet() - player.getBaseWallet() == 8
    }

    void testPlayerLoose() {
        Map<Machine.Symbols, Integer> gain = new HashMap<>()
        gain.put(Machine.Symbols.cherry, 0)
        Map<Machine.Symbols, Integer> symbols = new HashMap<>()
        symbols.put(Machine.Symbols.cherry, 1)
        Machine machine = new Machine(gain, symbols)
        Machine []machines = [machine]
        Room room = new Room(machines)

        Player player = new BasicPlayer()
        room.newPlayer(player)
        room.step(5)

        assert player.getWallet() - player.getBaseWallet() == -5
    }

    void testGetMachines()
    {
        Machine []machines = [new Machine(), new Machine(), new Machine()]
        Room room = new Room(machines)

        def returnMachine = room.getMachines()
        assert returnMachine.size() == machines.size()
        for (int i = 0; i < machines.size(); i++) {
            assert returnMachine[i] == machines[i]
        }
    }

}
