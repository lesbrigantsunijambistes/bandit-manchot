import java.util.Arrays;

/**
 * @author Julien Troussier
 *
 *
 */

public class MadDoubleHighSymbolsPlayer extends Player {

    private boolean played;

    MadDoubleHighSymbolsPlayer(){
        super();
        setBet(getBaseWallet());
    }

    MadDoubleHighSymbolsPlayer(Integer baseWallet){
        super(baseWallet);
        setBet(getBaseWallet());
    }

    @Override
    protected boolean[] getKept(Machine.Symbols[] result) {
        boolean kept[] = new boolean[result.length];
        Arrays.fill(kept, false);

        Integer tmpDoubleSymbol = -1;
        for (int i=0 ; i<kept.length ; i++) {
            for (int j=0 ; j<kept.length ; j++) {
                if(j != i && result[i].ordinal() == result[j].ordinal()) {
                    if(tmpDoubleSymbol == -1) {
                        tmpDoubleSymbol = result[i].ordinal();
                    }
                    else {
                        tmpDoubleSymbol = (result[i].ordinal() > tmpDoubleSymbol)?result[i].ordinal():tmpDoubleSymbol;
                    }
                }
            }
        }

        if(tmpDoubleSymbol != -1) {
            for (int i=0 ; i<kept.length ; i++) {
                if (result[i].ordinal() == tmpDoubleSymbol) {
                    kept[i] = true;
                }
            }
        }
        else {
            Integer tmpHighSymbol = 0;
            for (int i=0 ; i<kept.length ; i++) {
                if (tmpHighSymbol < result[i].ordinal()) {
                    tmpHighSymbol = result[i].ordinal();
                }
            }

            //Set true on kept array when the roll is on the high symbol
            for (int i=0 ; i<kept.length ; i++) {
                if (result[i].ordinal() == tmpHighSymbol) {
                    kept[i] = true;
                }
            }
        }

        played = true;

        return kept;
    }

    @Override
    public void updateReady() {
        super.updateReady();

        if(played) {
            setReady(false);
        }
    }
}
