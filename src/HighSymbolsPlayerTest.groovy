class HighSymbolsPlayerTest extends GroovyTestCase {
    void testGetKept() {
        FakeMachine machine = new FakeMachine()
        def p = new HighSymbolsPlayer()
        p.setMachine(machine)

        Machine.Symbols [][] setOfTest = [
                [Machine.Symbols.seven, Machine.Symbols.cherry, Machine.Symbols.bell],
                [Machine.Symbols.cherry, Machine.Symbols.bell, Machine.Symbols.watermelon],
                [Machine.Symbols.bell, Machine.Symbols.cherry, Machine.Symbols.bell]
        ]

        boolean [][] setOfResult = [
                [true, false, false],
                [false, true, false],
                [true, false, true]
        ]

        for (int i=0 ; i<setOfTest.length ; i++) {
            machine.setResult(setOfTest[i])
            p.play()
            def kept = machine.getTab();
            assert Arrays.equals(kept, setOfResult[i])
        }
    }

    void testUpdateReady() {
        def p1 = new HighSymbolsPlayer(10)
        def p2 = new HighSymbolsPlayer()
        initMachFullLose()
        p1.setMachine(mach)
        p2.setMachine(mach)

        for (int i=0 ; i<p1.getBaseWallet() ; i++) {
            assert p1.getReady() // Ready to play next draw x10
            p1.play()
        }
        assert !p1.getReady() // Haven't tokens to play next draw

        for (int i=0 ; i<p2.getBaseWallet() ; i++) {
            assert p2.getReady() // Ready to play next draw x10
            p2.play()
        }

        assert !p2.getReady() // Haven't tokens to play next draw
    }

    def mach

    void initMachFullLose() {
        def mreel = new HashMap<Machine.Symbols,Integer>()
        def gain = new HashMap<Machine.Symbols,Integer>()
        mreel.put(Machine.Symbols.seven, 3)
        gain.put(Machine.Symbols.seven, 0)
        mach = new Machine(gain, mreel)
    }
}
