class PlayerTest extends GroovyTestCase {
    void testMaxWallet() {
        def p = new BasicPlayer(10)
        initMachFullWin()
        p.setMachine(mach)

        p.play()            //Win 9 tokens
        assert p.getMaxWallet() == p.getWallet()
        int maxWallet = p.getWallet();

        initMachFullLose()
        p.play()
        assert p.getMaxWallet() == maxWallet
    }

    void testCountIteration() {
        FakeMachine machine = new FakeMachine()
        def p = new BasicPlayer()
        p.setMachine(machine)

        for (int i=0 ; i<10 ; i++) {
            p.play()
        }

        assert p.getCountIteration() == 10;
    }

    void testIsReadyToBegin() {
        def p = new BasicPlayer(10)
        assert !p.getReady() // Missing Machine

        FakeMachine machine = new FakeMachine()
        p.setMachine(machine);
        assert p.getReady() // OK

        p.setBet(20)
        assert !p.getReady() // Haven't tokens to play next draw

        p.setBet(1)
        assert p.getReady() // Ready to play next draw
    }

    def mach

    void initMachFullLose() {
        def mreel = new HashMap<Machine.Symbols,Integer>()
        def gain = new HashMap<Machine.Symbols,Integer>()
        mreel.put(Machine.Symbols.seven, 3)
        gain.put(Machine.Symbols.seven, 0)
        mach = new Machine(gain, mreel)
    }

    void initMachFullWin() {
        def mreel = new HashMap<Machine.Symbols,Integer>()
        def gain = new HashMap<Machine.Symbols,Integer>()
        mreel.put(Machine.Symbols.seven, 3)
        gain.put(Machine.Symbols.seven, 10);
        mach = new Machine(gain, mreel)
    }
}
