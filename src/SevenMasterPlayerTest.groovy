class SevenMasterPlayerTest extends GroovyTestCase {
    void testIsReadyToEnd() {
        def p = new SevenMasterPlayer(20)
        FakeMachine machine = new FakeMachine()
        p.setMachine(machine)
        machine.setResult([Machine.Symbols.seven, Machine.Symbols.seven, Machine.Symbols.bell] as Machine.Symbols[])

        assert p.getReady()
        p.play()
        assert p.getReady()

        machine.setResult([Machine.Symbols.seven, Machine.Symbols.seven, Machine.Symbols.seven] as Machine.Symbols[])

        assert p.getReady()
        p.play()
        assert !p.getReady()
    }

    void testKept() {
        FakeMachine machine = new FakeMachine()
        def p = new SevenMasterPlayer()
        p.setMachine(machine)

        Machine.Symbols [][] setOfTest = [
                [Machine.Symbols.cherry, Machine.Symbols.cherry, Machine.Symbols.seven],
                [Machine.Symbols.cherry, Machine.Symbols.seven, Machine.Symbols.watermelon],
                [Machine.Symbols.seven, Machine.Symbols.seven, Machine.Symbols.bell]
        ]

        boolean [][] setOfResult = [
                [false, false, true ],
                [false, true , false],
                [true , true , false]
        ]

        for (int i=0 ; i<setOfTest.length ; i++) {
            machine.setResult(setOfTest[i])
            p.play()
            def kept = machine.getTab();
            assert Arrays.equals(kept, setOfResult[i])
        }
    }
}
