class BasicPlayerTest extends GroovyTestCase {
    void testIsReadyToEnd() {
        def p = new BasicPlayer(10)
        initMachFullWin()
        p.setMachine(mach)

        assert p.getReady()
        p.play()            //Win 9 tokens
        p.play()            //Win again 9 token
        assert !p.getReady()

        p = new BasicPlayer(10)
        initMachFullLose()
        p.setMachine(mach)

        for (int i=0 ; i<10 ; i++) {
            assert p.getReady() // Ready to play next draw x10
            p.play()
        }

        assert !p.getReady() // Haven't tokens to play next draw


    }

    def mach

    void testKept() {
        FakeMachine machine = new FakeMachine()
        def p = new BasicPlayer()
        p.setMachine(machine)

        Machine.Symbols [][] setOfTest = [
                [Machine.Symbols.cherry, Machine.Symbols.cherry, Machine.Symbols.bell],
                [Machine.Symbols.cherry, Machine.Symbols.bell, Machine.Symbols.watermelon],
                [Machine.Symbols.bell, Machine.Symbols.cherry, Machine.Symbols.bell]
        ]

        for (Machine.Symbols [] test: setOfTest) {
            machine.setResult(test)
            p.play()
            def kept = machine.getTab();
            for (int i = 0; i < kept.length; i++) {
                assert kept[i] == false;
            }
        }
    }

    void initMachFullLose() {
        def mreel = new HashMap<Machine.Symbols,Integer>()
        def gain = new HashMap<Machine.Symbols,Integer>()
        mreel.put(Machine.Symbols.seven, 3)
        gain.put(Machine.Symbols.seven, 0)
        mach = new Machine(gain, mreel)
    }

    void initMachFullWin() {
        def mreel = new HashMap<Machine.Symbols,Integer>()
        def gain = new HashMap<Machine.Symbols,Integer>()
        mreel.put(Machine.Symbols.seven, 3)
        gain.put(Machine.Symbols.seven, 10);
        mach = new Machine(gain, mreel)
    }
}
